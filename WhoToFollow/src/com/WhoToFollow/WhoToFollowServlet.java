package com.WhoToFollow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.*;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class WhoToFollowServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		String topic = req.getParameter("Topic");
		topic = topic.concat("/");
		ArrayList<String> handles = new ArrayList<String>();
		// String topic = "politics/"; // should be the target topic.
		String url = "http://www.wefollow.com/interest/";
		url = url.concat(topic);
		URL wefollow = new URL(url);
		URLConnection yc = wefollow.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				yc.getInputStream()));
		String inputLine, html = "";
		String regx = "<p class=\"user-name\"><a href=\"/(.*?)\">(.*?)</a></p>";
		while ((inputLine = in.readLine()) != null)
			html += inputLine + "\n";
		Pattern pat = Pattern.compile(regx);
		Matcher mat = pat.matcher(html);
		while (mat.find()) {
			handles.add(mat.group(1));
			// resp.getWriter().println("@" + handle); // print the user's
			// handle
		}
		HttpSession session = req.getSession(true);
		session.setAttribute("handles", handles);
		resp.sendRedirect("WhoisResult.jsp");
		in.close();
	}
}
