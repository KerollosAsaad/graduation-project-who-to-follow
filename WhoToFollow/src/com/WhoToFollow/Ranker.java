package com.WhoToFollow;

public class Ranker {

	public Double rankTweet(Tweet t) {
		return Double.valueOf(t.numberOfFavorites + t.numberOfRetweets);
	}

}
