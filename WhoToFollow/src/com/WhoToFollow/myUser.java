package com.WhoToFollow;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

public class myUser implements Serializable {
	public long ID;
	public String name;
	public String ScreenName;
	public String biography;
	public int numberOfFollowers;
	public long numberOfFollowing;
	public String imageURL;
	public List<Tweet> tweets = new ArrayList<>();

	// public List <String> followingNames = new ArrayList<>();
	public myUser() {

	}
}
