package com.WhoToFollow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserStatistics implements Serializable {
	public long ID;
	public String name;
	public String ScreenName;
	public String biography;
	public String imageURL;
	public int numberOfFollowers;
	public long numberOfFollowing;

	public UserStatistics() {
	}

	public UserStatistics(myUser user) {
		ID = user.ID;
		name = user.name;
		ScreenName = user.ScreenName;
		biography = user.biography;
		imageURL = user.imageURL;
		numberOfFollowers = user.numberOfFollowers;
		numberOfFollowing = user.numberOfFollowing;
	}

}
