package com.WhoToFollow;

import java.io.Serializable;

public class Tweet implements Serializable {
	public String text;
	public String topic;
	public int numberOfFavorites;
	public int numberOfRetweets;
	public Double rank;

	public Tweet() {
	}

}
