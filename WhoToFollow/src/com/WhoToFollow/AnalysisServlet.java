package com.WhoToFollow;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.*;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

public class AnalysisServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/html;charset=UTF-8");
		String handle = req.getParameter("TwitterHandle");

		HELPER H = new HELPER();
		myUser user = H.getUser(handle);

		HttpSession session = req.getSession(true);
		//UserStatistics s = new UserStatistics(user);

		session.setAttribute("user", user);
		resp.sendRedirect("AnalysisResult.jsp");

		/*
		 * out.println("<!DOCTYPE html>"); out.println("<html>");
		 * out.println("<head>"); out.println("<title>Servlet myClass</title>");
		 * out.println("</head>"); out.println("<body>");
		 * 
		 * HELPER H = new HELPER(); myUser user = H.getUser(handle);
		 * 
		 * out.println("la2aaaaaaaaaaaa " + user.tweets.size() + "<br>");
		 * out.println("User Name : " + user.name + "<br>");
		 * out.println("User ID : " + user.ID + "<br>");
		 * out.println("Number of User Followers : " + user.numberOfFollowers +
		 * "<br>"); out.println("User Follow : " + user.numberOfFollowing +
		 * "<br>"); out.println("Last 200 Tweets <br>");
		 * 
		 * for (Tweet t : user.tweets) { out.print(t.text + "<br>");
		 * out.print("Topic : " + t.topic + "  ,  Fav. num : " +
		 * t.numberOfFavorites + "  ,  Retweet num : " + t.numberOfRetweets +
		 * "<br><br>"); }
		 * 
		 * 
		 * 
		 * out.println("</body>"); out.println("</html>"); out.close();
		 */
	}
}
