package com.WhoToFollow;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.google.appengine.api.ThreadManager;

public class HELPER {
	myTwitter twitter = new myTwitter();
	Classifier classifier = new Classifier();
	Ranker ranker = new Ranker();
	myUser user = new myUser();

	class RunnableDemo implements Runnable {
		public Thread t;
		public String threadName;
		int pack;
		String handle;

		RunnableDemo(String name) {
			threadName = name;
		}

		@Override
		public void run() {
			List<Tweet> l = twitter.getTweetPagkage(handle, pack);
			for (Tweet t : l) {
				t.topic = classifier.classifyTweet(t);
				t.rank = ranker.rankTweet(t);
			}
			user.tweets.addAll(l);
		}

		public void start() {
			if (t == null) {
				t = ThreadManager.createThreadForCurrentRequest(this);
				t.start();
			}
		}
	}

	public myUser getUser(String handle) {
		user.name = handle;
		user.ID = twitter.getUserID(user.name);
		user.numberOfFollowers = twitter.getNumberOfFollower(user.name);
		user.numberOfFollowing = twitter.getUserFriends(user.name);
		user.imageURL = twitter.getUserImage(user.ID);
		user.ScreenName = twitter.getUserScreenName(user.ID);
		user.biography = twitter.getUserBiography(user.ID);

		for (int k = 0; k <= 1; k++) {
			List<Tweet> l = twitter.getTweetPagkage(handle, k);
			for (Tweet t : l) {
				t.topic = classifier.classifyTweet(t);
				t.rank = ranker.rankTweet(t);
			}
			user.tweets.addAll(l);
		}

		/*
		 * List<RunnableDemo> runs = new ArrayList<>(); try { for(int
		 * k=1;k<=1;k++){ RunnableDemo R1 = new RunnableDemo( "Thread-" +
		 * String.valueOf(k)); R1.handle = handle; R1.pack=k; R1.start();
		 * runs.add(R1); } }catch (Exception e) {
		 * System.out.println("Failed to get timeline: " + e.getMessage()); }
		 * 
		 * for(RunnableDemo run:runs){ try { run.t.join(); } catch
		 * (InterruptedException ex) {
		 * 
		 * } }
		 */
		return user;
	}
}
