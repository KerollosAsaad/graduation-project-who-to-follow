package com.WhoToFollow;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

public class myTwitter {
	Twitter twitter = null;

	private void Init() {
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
				.setOAuthConsumerKey("0Vd2NGbMBoWbyRquX8irgTzmp")
				.setOAuthConsumerSecret(
						"Y7U1veaHSGTmoG7XfWcXEE1wv9RcqFhIUSwWyIJZk3h91zlMFh")
				.setOAuthAccessToken(
						"2917644977-I4uNEIdsl817gxRzCiLgCkuvxOjcbetNkWAdDWc")
				.setOAuthAccessTokenSecret(
						"N6qsRK3anLrN7DWvp7d2ZIivlAhzz4C3pmzmvRyfHOGia");

		TwitterFactory tf = new TwitterFactory(cb.build());
		twitter = tf.getInstance();
	}

	public myTwitter() {
		if (twitter == null)
			Init();
	}

	public long getUserID(String name) {
		if (twitter == null)
			Init();
		long id = -1;
		try {
			User u = twitter.showUser(name);
			id = u.getId();
		} catch (Exception e) {
		}
		return id;
	}

	public long getUserFriends(String name) {
		if (twitter == null)
			Init();
		long friends = 0;
		try {
			User u = twitter.showUser(name);
			friends = u.getFriendsCount();
		} catch (Exception e) {
		}
		return friends;
	}

	public String getUserImage(long id) {
		try {
			User user = twitter.showUser(id);
			String url = user.getOriginalProfileImageURL();
			return url;
		} catch (Exception e) {
		}
		return "";
	}

	public String getUserScreenName(long id) {
		try {
			User user = twitter.showUser(id);
			String name = user.getName();
			return name;
		} catch (Exception e) {
		}
		return "";
	}

	public int getNumberOfFollower(String name) {
		if (twitter == null)
			Init();
		int numberOfFollowers = 0;
		try {
			User u = twitter.showUser(name);
			numberOfFollowers = u.getFollowersCount();
		} catch (Exception e) {
		}
		return numberOfFollowers;
	}

	public String getUserBiography(long id) {
		if (twitter == null)
			Init();
		try {
			User u = twitter.showUser(id);
			return u.getDescription();
		} catch (Exception e) {
		}
		return "";
	}

	public List<Tweet> getTweetPagkage(String user, int k) {
		if (twitter == null)
			Init();
		List<Tweet> tweets = new ArrayList<>();
		try {
			List<Status> st = twitter.getUserTimeline(user, new Paging(k, 200));
			for (Status s : st) {
				Tweet t = new Tweet();
				t.text = s.getText();
				t.numberOfFavorites = s.getFavoriteCount();
				t.numberOfRetweets = s.getRetweetCount();
				tweets.add(t);
			}
		} catch (Exception e) {
		}
		return tweets;
	}

	public List<Tweet> getUserTweets(String name) {
		if (twitter == null)
			Init();
		List<Tweet> tweets = new ArrayList<>();
		for (int k = 1; k <= 1; k++) { // TODO
			tweets.addAll(getTweetPagkage(name, k));
		}
		return tweets;
	}

}
