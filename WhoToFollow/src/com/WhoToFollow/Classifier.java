package com.WhoToFollow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import twitter4j.JSONException;
import twitter4j.JSONObject;

public class Classifier {
	public static String RemoveURLS(String s) {
		return s.replaceAll("http.*?\\s", "");
	}

	public static String encode(String s) {
		s = RemoveURLS(s);
		String res = "";
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == ' ' || s.charAt(i) == '#')
				res = res + "%20";
			else
				res = res + s.charAt(i);
		}
		return res;
	}

	public String classifyText(String text) {
		String topic = "un-defined";
		double mx = -1;

		String ReadKey = "f7gz9PvVqBfuHVJAGSBtfNiX8Q";
		text = encode(text);

		// String
		// myClassifier="http://www.uclassify.com/browse/joseph/TwitterClassifier/ClassifyText?readkey="+
		// ReadKey +"&text="+ text +"&output=json&version=1.01";
		String uClassifyClassifier = "http://www.uclassify.com/browse/uClassify/Topics/ClassifyText?readkey="
				+ ReadKey + "&text=" + text + "&output=json&version=1.01";
		try {
			URL url = new URL(uClassifyClassifier);
			URLConnection conn = url.openConnection();
			InputStream in = conn.getInputStream();
			InputStreamReader reader = new InputStreamReader(in);
			BufferedReader bf = new BufferedReader(reader);
			String s, data = "";

			while ((s = bf.readLine()) != null)
				data = data + s + "\n";

			JSONObject obj = new JSONObject(data).getJSONObject("cls1");
			Iterator<String> keys = obj.keys();
			while (keys.hasNext()) {
				String key = keys.next();
				double value = Double.parseDouble(obj.get(key).toString());
				if (value > mx) {
					mx = value;
					topic = key;
				}
			}
		} catch (Exception e) {
			return "un-defined";
		}

		if (mx < 0.3)
			return "un-defined";
		return topic;
	}

	public String classifyTweet(Tweet t) {
		return classifyText(t.text);
	}

	public void classifyUser(myUser user) {

	}

	public void classifyTweets(List<Tweet> L) {

	}
}
