<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Who To Follow</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/five.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,600,600italic' rel='stylesheet' type='text/css'>



</head>

<body id="page-top" class="index" style="background-color:#67ACFB">




<nav class="navbar navbar-fixed-top navbar-inverse">
  <div class="container"> 
  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="hidden"> <a href="#page-top"></a> </li>
        <li> <a class="page-scroll" href="index.html">HOME</a> </li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>



<section id="Expert" style="background-color:#67ACFB">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading"> Who is the expert </h2>
        <div class="line"></div>
      </div>
    </div>
    
    <div class="row">
    	<p class="text-jo" > See who's the expert in a given topic </p>
    </div>
    <form action="WhoToFollowServlet" method="get" >
       <div class="form-group" >
    	<div class="row">
     		<div class="col-sm-2"></div>
    		<div class="col-sm-6">
      			<div class="input-group input-group-lg">
  					<span class="input-group-addon" id="basic-addon1"></span>
  					<input type="text" name="Topic" class="form-control" placeholder="Enter a topic (ie. Politics)" >
	  			</div>
	  		</div>
	  		<div class="col-sm-2">
		  	   	<button class="btn-lg btn-defult" type="submit"  > Forward </button>
	  	  	</div>
	  		<div class="col-sm-2"></div>
    	</div>
    	</div>
    </form>
    
  </div>
</section>


</body>
</html>
