<%@page import="com.WhoToFollow.Tweet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.WhoToFollow.myUser"%>
<%@page import="com.WhoToFollow.HELPER"%>
<%@page import="com.WhoToFollow.UserStatistics"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Who To Follow</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/five.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700"
	rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Kaushan+Script'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,600,600italic'
	rel='stylesheet' type='text/css'>



</head>


<body style="background-color: #f7f7f7">


	<%
		myUser user = (myUser) request.getSession().getAttribute("user");
		//UserStatistics user = (UserStatistics) request.getSession()
		//		.getAttribute("stat");
		String url = "https://twitter.com/" + user.name;
	%>

	<nav class="navbar navbar-fixed-top navbar-inverse">
	<div class="container">

		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li class="hidden"><a href="#page-top"></a></li>
				<li><a class="page-scroll" href="index.html">HOME</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid --> </nav>



	<section id="Analysis" style="background-color:#f7f7f7">
	<div class="container">
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-2">
				<img src=<%=user.imageURL%>
					style="width: 160px; height: 160px; box-shadow: 0 0 8px rgba(0, 0, 0, .8);">
			</div>
			<div class="col-sm-5">
				<h2 style="color: #000000"><%=user.ScreenName%></h2>
				@<%=user.name%>

				<a href=<%=url%> target="_blank"><i class="fa fa-twitter"></i></a>
				<h5>
					<%=user.biography%>
					<h6>
						Followers
						<%=user.numberOfFollowers%>
					</h6>
					<h6>
						Following
						<%=user.numberOfFollowing%>
					</h6>
				</h5>
				<h6>
					<%
						for (Tweet t : user.tweets) {
							out.print(t.text + "<br>");
							out.print("Topic : " + t.topic + "  ,  Fav. num : "
									+ t.numberOfFavorites + "  ,  Retweet num : "
									+ t.numberOfRetweets + "<br>----------<br>");
						}
					%>

				</h6>
			</div>

		</div>
	</div>
	</section>


</body>


</html>
